const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		unique: true
		// required: [true, "Name is required"]
	},
	brand: {
		type: String
	},
	image: {
		type: String 
		// data: Buffer, 
		// contentType: String
	},
	description: {
		type: String
		// required: [true, "Description is required"]
	},
	category: {
		type: String
	},
	price: {
		type: Number,
		default: 1000
	},
	quantity: {
		type: Number,
		default: 3
	},
	isActive: {	
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	productOrders: [{
		orderId: {
			type: mongoose.Types.ObjectId,
			ref: "Order"
		},
		_id: false
	}]
});

module.exports = mongoose.model("Product", productSchema);