const mongoose = require('mongoose');


const cartSchema = new mongoose.Schema(
  {
  userId: { type: String, required: true },
  email: { type: String, required: true },
  products: [{
   name: { type: String },
	brand: { type: String },
	image: { type: String },
	description: { type: String },
	category: { type: String },
   price: { type: Number, default: 0 }, 
   quantity: { type: Number, default: 1 },
   productId: {type: String,},
   subTotal: { type: Number, default: 0 }, 
   _id : false,
  }
  ]
},
{timestamps: true}
  )

module.exports = mongoose.model('Cart', cartSchema);