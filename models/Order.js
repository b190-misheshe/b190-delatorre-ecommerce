const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	email: {
		type: String,
		required: true
	},
	userId: {
		type: String,
		required: true
	},
	products:[{
		productId: { type: String,},
		item: {
				type: String,
			},
		name: {
				type: String,
			},
		price: {
				type: Number,
				// default: 1000,
			},
		quantity: {
				type: Number,
				default: 1,
			},
		subTotal: {
			type: Number,
			default: 0
		},
	}],
	totalAmount: {
		type: Number,
		default: 0
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	status: {
		type: String,
		default: "pending"
	},
});

module.exports = mongoose.model("Order", orderSchema);