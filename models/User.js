const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: [true, "Input field required."]
	},
	email: {
		type: String,
		unique: true,
		required: [true, "Input field required."]
	},
	password: {
		type: String,
		minlength: 7,
		required: [true, "Input field required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		unique: true,
	},
	userOrders: [{
		orderId:{
			type: mongoose.Types.ObjectId,
			ref: "Order",
		},
		_id: false,
		productId:{
			type: mongoose.Types.ObjectId,
			ref: "Product",
		},
		_id: false
	}]},
	{timestamps: true}
);

module.exports = mongoose.model("User", userSchema);