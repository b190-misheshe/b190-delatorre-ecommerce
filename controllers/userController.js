const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");
const Cart = require("../models/Cart");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");
// **********

// Check email exists
module.exports.checkEmailExists = ( reqBody ) => {
	return User.find( { email: reqBody.email } ).then( result => {
		if (result.length > 0){
			return true;
		} else {
			return false;
		}
	} )
};

// User registration
module.exports.registerUser = (reqBody) => {
   let newUser = new User (
	{ fullName: reqBody.fullName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        mobileNo: reqBody.mobileNo,
	});
      return newUser.save().then((data, error) => {
        if (error) {
				return false;
        } else {
				let newCart =new Cart(
				{	email: reqBody.email,
					userId: data._id
				})
				newCart.save()
				return true;
        }
})
};

// User authentication
module.exports.loginUser = ( reqBody) => {
	return User.findOne({email: reqBody.email}).then((result, user) =>{
		if(result === null){
			return false;
			// return "Invalid email or password.";
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
				// return ({message: "Logged in.", access: auth.createAccessToken(result)})
			} else {
				return false;
				// return "Invalid email or password.";
			}
		}
	} )
} 

// Set user as admin
module.exports.setAdmin = (id, isAdmin) => {
	let setAsAdmin = { isAdmin : true };
	let setAsNonAdmin = { isAdmin : false };
	if (isAdmin === true) {
		return User.findByIdAndUpdate(id, setAsNonAdmin).then((res, error) => {
			if (error) {
				  return false;
		  } else {
				 return true;
		  }
	});
	} else {
		return User.findByIdAndUpdate(id, setAsAdmin).then((res, error) => {
			if (error) {
				  return false;
		  } else {
				 return true;
		  }
	});
	}
};

// Retrieve all registered users
module.exports.getAllUsers = () => {
	return User.find({}, {address: 0, __v:0, password: 0}).then(result => {
//   return User.find().then(result => {
	return result;
   //  return ({registeredUsers});
  })
}

// ******

// // Create Order / Non-Admin User Checkout
// module.exports.createOrder = (data) => {
// 	return Product.findById(data._id).then(res =>{
// 		// console.log(res)

// 		let newOrder = new Order ({
// 			userId: data.userId,
// 			email: data.email,
// 			item: res.name,
// 			productId: data._id,
// 			price: res.price,
// 		});
// 		return newOrder.save().then((order, error) => {
// 			// console.log(order);
// 			if (error) {
// 				return false;
// 			} else {
// 				return true;
// 			}})
// 		})}


//CREATE ORDER / CHECKOUT 2
module.exports.checkout = async (userData) => {
	const userProduct = await Cart.findOne({userId: userData.userId}).select({'products': 1, '_id': 0});
	let newOrder = new Order(
	  { userId: userData.userId,
		 email: userData.email });
 	const savedOrder = await newOrder.save();
 	await Order.updateOne(
		{_id: savedOrder.id},
		{$push: {products : {$each: userProduct.products}}}
	);
	await Order.aggregate([
		{ $match: {_id: savedOrder._id} },
		{ $addFields: { totalAmount: { $sum: "$products.subTotal"}}},
		{ $merge: { into: "orders" }}
	]);
 	return Cart.findOneAndDelete({userId: userData.userId}).then((res, error) => {
	  	if (error) {
			return false
	  	} else {
				let newCart = new Cart({ userId: userData.userId, email: userData.email })
				newCart.save()
				return true;
			}
 	})
}



// Retrieve all orders
module.exports.viewAllOrders = () => {
  return Order.find().then(result => {
	return result;
   //  return ({allOrders});
  })
}

// Retrieve authenticated user's orders
module.exports.viewOrders = (userData) => {
	return Order.find({userId: userData}).then(orders => {
		if (orders.length === 0) {
			return false;
		} else {
			return true;
		}
	})
}

// Retrieve user's order
module.exports.getOrder = (userData) => {
	return Order.findById(userData).then(orders => {
		if (orders.length === 0) {
			return false;
		} else {
			return orders;
		}
	})
}

// ****

module.exports.getProfile = (data) => {
	// console.log(data);
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

// ADD TO CART
module.exports.addToCart = async (userId, product) => {
	const purchasedItem = await Product.findById(product.id);
	console.log(purchasedItem)
	const userCart = await Cart.find({ userId: userId, 'products.productId': product.id });
	if ( userCart.length === 0 ) {
		await Cart.findOneAndUpdate({ userId: userId },
			{ $push: { 'products': { 
											productId: product.id, 
											name: purchasedItem.name,
											brand: purchasedItem.brand,
											image: purchasedItem.image,
											price: purchasedItem.price,
											quantity: product.quantity,
											subTotal: parseInt((purchasedItem.price*product.quantity).toFixed(2) )
													}		
				}})
		return true
} else if ( userCart.length === 1 ) {
		await Cart.findOneAndUpdate({ userId: userId, 'products.productId': product.id },
		{ $inc : {'products.$.quantity' : product.quantity, 'products.$.subTotal' : purchasedItem.price*product.quantity }})
		return true;
} else {
	return false
}
}

