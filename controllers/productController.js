const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
// **********

// Create product (admin only) = (/add)
module.exports.createProduct = (product) => {
  let newProduct = new Product({
      name : product.name,
      brand : product.brand,
      image : product.image,
      description : product.description,
      category : product.category,
      price : product.price,
      quantity: product.quantity,
  });
  return newProduct.save().then((product, error) => {
    if (error) {
      return false;
      // return ({error: "Failed to create new product"});
    } else {
      console.log(product);
      return true;
      // return ({success: "Created a new product.", product});
    }
  })
}

// Retrieve all active products = (/active)
module.exports.getActiveProducts = () =>{
	return Product.find({isActive: true}).then(result =>{
    return result;
	})
};


// Retrieve all products = (/active)
module.exports.getAllProducts = () =>{
	return Product.find({}).then(result =>{
    return result;
	})
};

// Retrieve single products  = (/:productId)
module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result =>{
		return result;
	})
};

// Update product information (admin only) = (/update/productId)
module.exports.updateProduct = (reqBody) => {
  let updatedProduct =  {
    name : reqBody.name,
    brand : reqBody.brand,
    description: reqBody.description,
		quantity: reqBody.quantity, 
		price: reqBody.price
  };
  console.log(updatedProduct)
  console.log(reqBody.id)
	return Product.findByIdAndUpdate(reqBody.id, updatedProduct).then((product, error) => {
		if (error) {
      return false;
    } else {
      console.log(product);
      return true;
    }
  })
}

// Archive product (admin only)
module.exports.archiveProduct = ( id, isActive ) => {
  console.log(id, isActive);
  let active = { isActive : true };
  let inactive = { isActive : false };
  if ( isActive === true) {
    return Product.findByIdAndUpdate( id, inactive ).then(( res, error ) => {
      if ( error ) {
          return false;
      } else {
          return true;
      }
    });
  } else {
    return Product.findByIdAndUpdate( id, active ).then(( res, error ) => {
      if ( error ) {
          return false;
      } else {
          return true;
      }
    });
  };
}
// ****







