const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();

const productRoutes = require("./routes/productRoutes");
const userRoutes = require("./routes/userRoutes");
const cartRoutes = require("./routes/cartRoutes");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/cart", cartRoutes);

mongoose.connect("mongodb+srv://misheshe:admin@wdc028-course-booking.jj30hwt.mongodb.net/b190-delatorre-ecommerce?retryWrites=true&w=majority",
{
	useNewUrlParser : true,
	useUnifiedTopology : true
}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the database"));


app.listen(process.env.PORT || 4000, () => {console.log(`API now online at port ${process.env.PORT || 4000}`)});