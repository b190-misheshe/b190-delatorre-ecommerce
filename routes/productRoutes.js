const express = require("express");
const router = express.Router();
const cors = require("cors");

const productController = require("../controllers/productController");

const auth = require("../auth.js");
// **********

// Create product
router.post("/", auth.verify, (req, res) => {
  if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
    // return false;
	} else {
  productController.createProduct(req.body)
  .then(resultFromController => res.send(resultFromController));
  }
});

// Retrieve all active products
router.get("/", (req, res) => {
  productController.getActiveProducts()
  .then(resultFromController => res.send(resultFromController));
});

// Retrieve all products
router.get("/all", (req, res) => {
  productController.getAllProducts()
  .then(resultFromController => res.send(resultFromController));
});

// Retrieve single products
router.get("/:id", (req, res) => {
  productController.getProduct(req.params.id)
  .then(resultFromController => res.send(resultFromController));
});

// Update product information (admin only)
router.put("/", auth.verify, (req, res) => {
  if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
    console.log(req.body)
  productController.updateProduct(req.body)
  .then(resultFromController => res.send(resultFromController));
  }
});

// Archive product (admin only)
router.put("/archive", auth.verify, (req, res) => {
  if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
  productController.archiveProduct(req.body.id, req.body.isActive)
  .then(resultFromController => res.send(resultFromController));
  }
});


// ****

module.exports = router;

