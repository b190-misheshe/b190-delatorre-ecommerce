const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cartController');
const auth = require("../auth.js");

//VIEW CART
router.get( "/", auth.verify, ( req, res ) => {
	console.log("hello there")
	let userData = auth.decode(req.headers.authorization ).id
	cartController.viewCart( userData )
	.then( resultFromController => res.send( resultFromController ));
});

//REMOVE FROM CART
router.post( "/remove", auth.verify, ( req, res ) => {
	let userId = auth.decode(req.headers.authorization ).id

	cartController.removeFromCart( userId, req.body.productId )
	.then( resultFromController => res.send( resultFromController ));
});



module.exports = router