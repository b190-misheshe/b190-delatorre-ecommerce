const express = require("express");
const router = express.Router();
const cors = require("cors");

const userController = require("../controllers/userController");

const auth = require("../auth.js");
// ***************

// Check EMail Exists
router.post('/checkEmail', ( req, res ) =>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
} )

// User registration
router.post("/register", (req, res) => {
	userController.registerUser( req.body ).then(resultFromController => res.send(resultFromController));
})

// Retrieve all registered users
router.get("/", (req, res) => {
	userController.getAllUsers(req.body)
	.then(resultFromController => res.send(resultFromController));	
});

// User authentication
router.post("/login", (req, res) => {
	  userController.loginUser(req.body)
	  .then(resultFromController => res.send(resultFromController));
});
	
// Set user as admin
router.put("/setAdmin/", auth.verify,(req,res) => {
	if (auth.decode(req.headers.authorization).isAdmin) {
	  userController.setAdmin(req.body.id, req.body.isAdmin)
	  .then(result => res.send (result));
	} else {
	  res.status(401).send({Error: "Unauthorized request."})
	}
 });


//ADD TO CART
router.post( '/addToCart', auth.verify, ( req, res ) => {
	let userId = auth.decode( req.headers.authorization ).id
	userController.addToCart( userId, req.body )
  .then( resultFromController => res.send( resultFromController ));
});

// Create Order / Non-Admin User Checkout
router.post("/checkout", auth.verify, (req, res) => {
  let userData = {
    userId: auth.decode(req.headers.authorization).id,
    email: auth.decode(req.headers.authorization).email,
  }
  userController.checkout(userData)
  .then(resultFromController => res.send(resultFromController));
});

// Retrieve all orders
router.get("/orders", (req, res) => {
		userController.viewAllOrders()
		.then(resultFromController => res.send(resultFromController));
});

// Retrieve authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization).id;
	userController.viewOrders(userData)
	.then(resultFromController => res.send(resultFromController));
});

router.get("/orderAdmin/:_id", (req, res) => {
	userController.getOrder(req.params)
	.then(resultFromController => res.send(resultFromController));
});

router.get("/details", auth.verify, (req, res) => {
	// 
	const userData = auth.decode(req.headers.authorization);
	// console.log(userData);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// ****

module.exports = router;










// ***** NOTES *****

// USERS
// User registration = (/register)
// User authentication = (/login)
// Set user as admin (admin only) = (newAdmin/:userId)
// Retrieve all registered users = (/all)

// PRODUCTS
// Create product (admin only) = (/add)
// Retrieve single product = (/:productId)
// Retrieve all active products = (/active)
// Update product information (admin only) = (/update/productId)
// Archive product (admin only) = (/archive/productId)

// ORDERS
// Non-admin user checkout (create order) = (/add)
// Retrieve authenticated user's orders | by orderId= (/:orderId)
// Retrieve authenticated user's orders | by userId = (/)
// Retrieve all orders (admin only) = (/all)

// up
